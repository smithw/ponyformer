import greenfoot.*;

public class RainbowDashPower extends Power {
	public static double kSpeedMultiplier = 10.0;
	public static double kGMultiplier = -2.0;
	private static GreenfootSound sfx = null;

	protected GreenfootSound getSFX() {
		if (sfx == null) sfx = new GreenfootSound("rainbowdash-power.mp3");

		return sfx;
	}


	protected boolean shouldDeactivate(Pony actor) {
		return Math.abs(actor.getMotion(Axis.X).getSpeed()) <= PonyVille.kDefaultSpeed;
	}

	protected void perform(Pony actor) {
		Motion current;
		UniformlyAcceleratedMotion next;
		double direction;

		current = actor.getMotion(Axis.X);
		next = current.transitionInto(UniformlyAcceleratedMotion.class, false);

		direction = (double) actor.getFacing();

		next.rawSetPosition(((ScrollingWorld) actor.getWorld()).getPosition());
		next.setSpeed(kSpeedMultiplier * direction * PonyVille.kDefaultSpeed);
		next.setAcceleration(kGMultiplier * direction * PonyVille.kDefaultG);

		actor.setMotion(Axis.X, next);
	}

	protected void unperform(Pony actor) {
		Motion current;
		UniformLinearMotion next;

		current = actor.getMotion(Axis.X);
		next = current.transitionInto(UniformLinearMotion.class, false);
		next.rawSetPosition(((ScrollingWorld) actor.getWorld()).getPosition());

		actor.setMotion(Axis.X, next);
	}
}