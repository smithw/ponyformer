import greenfoot.*;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.ArrayList;

public abstract class Motion {
	protected ExtendedActor boundActor;
	protected double speed;
	protected double position;
	protected Axis axis;

	private Object speedLock;
	private Object positionLock;

	public Motion(ExtendedActor boundActor, Axis axis) {
		speedLock = new Object();
		positionLock = new Object();
		
		bindActor(boundActor);
		setAxis(axis);
		setSpeed(0.0);
	}

	public void resetPosition() {
		setPosition(axis.get(boundActor));
	}

	public void setAxis(Axis newAxis) {
		this.axis = newAxis;
	}
	
	protected final void bindActor(ExtendedActor actor) {
		boundActor = actor;
	}

	public void setSpeed(double newSpeed) {
		synchronized (speedLock) {
			this.speed = newSpeed;
		}
	}

	public double getSpeed() {
		double speed;

		synchronized (speedLock) {
			speed = this.speed;
		}

		return speed;
	}

	public void rawSetPosition(double newPosition) {
		synchronized (positionLock) {
			this.position = newPosition;
		}
	}

	public void setPosition(double newPosition) {
		synchronized (positionLock) {
			if (newPosition != this.position) {
				this.position = newPosition;
				axis.set(boundActor, (int) this.position);
			}
		}
	}
	
	public double getPosition() {
		double position;

		synchronized (positionLock) {
			position = this.position;
		}

		return position;
	}

	public void step(long deltaT) {
		setSpeed(speedEquation(deltaT));

		setPosition(getPosition() + getSpeed() * (double) deltaT);
	}

	public void stepBack(CollisionType coll) throws IllegalStateException {
		int direction = (int) (getSpeed() / Math.abs(getSpeed()));

		if (direction == 0) {
			if ((coll == CollisionType.BOUNDARY) &&
				(axis == Axis.Y) &&
				(boundActor.getCollisionBoundaries(axis).get(0) == Boundary.START) ) {
				// Colisão com o teto precisa de tratamento especial; há casos em
				// que a velocidade será zero (por causa da aceleração da gravidade)
				// e, mesmo assim, precisamos garantir que o ator não ficará "preso"
				// no teto.
				direction = 1;
			}
			else throw new IllegalStateException();
		}

		setPosition(getPosition() - direction);
	}

	public final <T extends Motion> T transitionInto(Class<T> newCls) {
		return transitionInto(newCls, true);
	}

	public final <T extends Motion> T transitionInto(Class<T> newCls, boolean reset) {
		T newMotion;
		try {
			Constructor<T> c = newCls.getConstructor(ExtendedActor.class, Axis.class);
			newMotion = c.newInstance(boundActor, axis);
			if (reset) newMotion.resetPosition();
			newMotion.setSpeed(getSpeed());
			return newMotion;
		}
		catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	protected abstract double speedEquation(long deltaT);
}