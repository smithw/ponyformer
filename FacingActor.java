import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class FacingActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class FacingActor extends KillableActor {
	/**
	 * Act - do whatever the FacingActor wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */
	protected int facing;

	public FacingActor() {
		facing = 1;
	}

	public FacingActor(int facing) {
		this.facing = facing;
	}

	public void faceRight() {
		if (facing < 0) {
			getImage().mirrorHorizontally();
			facing = 1;
		}
	}

	public void faceLeft() {
		if (facing > 0) {
			getImage().mirrorHorizontally();
			facing = -1;
		}
	}

	public void faceDirection(int dir) {
		if (dir > 0) faceRight();
		else if (dir < 0) faceLeft();
		else faceDirection(facing);
	}

	public int getFacing() {
		return facing;
	}
}
