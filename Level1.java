import greenfoot.*;

public class Level1 implements Level {
	public void makeLevel(World w) {
			LevelMaker lm = new LevelMaker(w);
			Box hills = lm.makeHills(4, 5, 800);
			Box jewels = lm.makeJewels(10, hills.getLeftX(), 0).addOver(hills);
			Box enemy = lm.makeEnemyWithBouncers(0, 0, 400).addOver(hills).addAfter(jewels, 30);
			Box valley1 = lm.makePlatform(0, 400).addAfter(hills);
			Box valley2 = lm.makePlatforms(8, 0, 0).addAfter(valley1).addOnFloor(w);
			Box valley3 = lm.makeHPlatforms(4, 0, 0).addAfter(valley2).addOnFloor(w);
			Box jewels2 = lm.makeJewels(40, 0, 0).addOver(valley2).addAfter(valley1);
			Box jewels3 = lm.makeJewels(5, valley3.getLeftX(), 0).addOver(valley3);
			Box enemies = lm.makeMultipleEnemiesWithBouncers(5, 0, 0, 400).addAfter(valley1).addOver(valley2);
			Box trap = lm.makeFluttershySpikeTrap(0).addAfter(valley3);
			Box end = lm.makeEndOfLevel(0).addAfter(trap, 400);

			hills.addToWorld(w);
			jewels.addToWorld(w);
			jewels2.addToWorld(w);
			jewels3.addToWorld(w);
			enemy.addToWorld(w);
			valley1.addToWorld(w);
			valley2.addToWorld(w);
			valley3.addToWorld(w);
			enemies.addToWorld(w);
			trap.addToWorld(w);
			end.addToWorld(w);
	}
}