import greenfoot.*;

public class Level2 implements Level {
	public void makeLevel(World w) {
			LevelMaker lm = new LevelMaker(w);
			Box plat1 = lm.makePlatform(1000, 0).addOnFloor(w);
			Box bombs = lm.makeSpikes(10, 0, 0).addOnFloor(w).addAfter(plat1);
			Box plat2 = lm.makePlatform(0, 0).addOnFloor(w).addAfter(bombs);
			Box plat3 = lm.makeHPlatforms(3, 0, 0).addOnFloor(w).addAfter(plat2);
			Box topPlats = lm.makeHSpikes(5, 0, 0).addAfter(plat3, 280);
			topPlats.setTopY(1);
			Box enemy1 = lm.makeEnemyWithBouncers(0, 0, 400).addAfter(plat3).addOnFloor(w);
			Box highGround = lm.makeHPlatforms(5, 0, 0).addOnFloor(w).addAfter(enemy1, 250);
			Box highGround2 = lm.makeHills(4, 2, 0).addAfter(highGround);
			Box enemies = lm.makeMultipleEnemiesWithBouncers(2, 0, 0, 400).addAfter(highGround).addOver(highGround2);
			Box highGround3 = lm.makeHills(4, 5, 0).addAfter(highGround2);
			Box trap = lm.makeFluttershySpikeTrap(0).addAfter(highGround3);
			Box end = lm.makeEndOfLevel(0).addAfter(trap, 300);

			Box j1 = lm.makeJewels(5, 0, 0).addAfter(plat2).addOver(plat3);
			Box j2 = lm.makeJewels(20, 0, 0).addAfter(highGround).addOver(highGround2);
			Box j3 = lm.makeJewels(20, 0, 0).addAfter(highGround2).addOver(highGround3);


			plat1.addToWorld(w);
			plat2.addToWorld(w);
			plat3.addToWorld(w);
			topPlats.addToWorld(w);
			bombs.addToWorld(w);
			enemy1.addToWorld(w);
			highGround.addToWorld(w);
			highGround2.addToWorld(w);
			enemies.addToWorld(w);
			highGround3.addToWorld(w);
			trap.addToWorld(w);
			end.addToWorld(w);
			j1.addToWorld(w);
			j2.addToWorld(w);
			j3.addToWorld(w);
	}
}