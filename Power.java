import greenfoot.*;

public abstract class Power {
	protected abstract void perform(Pony actor);
	protected abstract void unperform(Pony actor);

	protected abstract GreenfootSound getSFX();

	protected boolean isUseable(Pony actor) {
		return true;
	}

	protected boolean shouldDeactivate(Pony actor) {
		return false;
	}

	public void use(Pony actor) {
		if (!actor.getIsPowerActive() ) {
			if (isUseable(actor)) {
				GreenfootSound sfx = getSFX();

				actor.setIsPowerActive(true);
				perform(actor);

				if (sfx != null) {
					if (sfx.isPlaying()) sfx.stop();
					sfx.play();
				}
			}
		}
		else if (shouldDeactivate(actor)) deactivate(actor);
	}

	public void deactivate(Pony actor) {
		if (actor.getIsPowerActive()) {
			unperform(actor);
			actor.setIsPowerActive(false);
		}
	}
}