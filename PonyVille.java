import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;
import java.lang.Class;
import java.lang.reflect.Method;

/**
 * Write a description of class PonyVille here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PonyVille extends ScrollingWorld {

	/**
	 * Constructor for objects of class PonyVille.
	 * 
	 */
	public static double kDefaultSpeed = 0.5;
	public static double kJumpHeight   = 1.8;
	public static double kDefaultG     = 0.005;

	private GameState gameState;
	private Score score;
	private boolean changingState;
	private boolean wonLastLevel;
	private int lastScore;

	private static Class[] levels = {
		Level1.class,
		Level2.class
	};

	private int currentLevel = 0;

	public PonyVille() {
		// Create a new world with 600x400 cells with a cell size of 1x1 pixels.

		super(1200, 800, 1); 
		setGameState(GameState.Intro1);
		changingState = false;

		setWonLastLevel(false);
	}

	public void setWonLastLevel(boolean newWonLastLevel) {
		this.wonLastLevel = newWonLastLevel;
	}
	
	public boolean getWonLastLevel() {
		return this.wonLastLevel;
	}

	public void makeLevel(Class level) {
		try {
			Method m = level.getMethod("makeLevel", World.class);
			Level obj = (Level) level.newInstance();

			m.invoke(obj, this);
		}
		catch (Exception e) {
			System.out.println("Fatal error");
			Greenfoot.stop();
		}
	}

	public void started() {
		getGameState().getBGM().playLoop();
		setBouncerTransparency(0);
	}

	public void stopped() {
		getGameState().getBGM().pause();
		setBouncerTransparency(255);
	}

	public void setBouncerTransparency(int t) {
		for (Object b : getObjects(MonsterBouncer.class))
			((MonsterBouncer) b).getImage().setTransparency(t);
	}

	public void changeState(GameState state) {
		GameState current = getGameState();

		if (score != null) lastScore = score.getScore();
		score = null;

		removeObjects(getObjects(null));

		if ((state == GameState.Game) && (currentLevel >= levels.length)) {
			currentLevel = 0;
			state = GameState.Intro1;
			lastScore = 0;
		}

		if (state != GameState.Game) {
			setStart(0);
			setPosition(0);
			setEnd(0);
		}

		if (current.getBGMName() != state.getBGMName()) {
			current.getBGM().stop();
			state.getBGM().playLoop();
		}

		setGameState(state);

		if (state == GameState.Game) startGame();
		else if (state == GameState.PostGame) showPostGame();
		else if (state == GameState.GameOver) showGameOver();
	}

	public void startGame() {
		score = new Score((getWonLastLevel()) ? lastScore : 0);
		addObject(score, 170, 45);
		setEnd(-1);

		Pony p = new Pony();
		addObject(p, 400, 200);

		setWonLastLevel(false);

		makeLevel(levels[currentLevel]);
	}

	public boolean updateScore() {
		if (UserInfo.isStorageAvailable()) {
			System.out.println("ok");
			UserInfo info = UserInfo.getMyInfo();
			if (lastScore > info.getScore()) {
				info.setScore(lastScore);
				info.store();
			}
		}
		else System.out.println("not ok");

		return false;
	}

	public void showInfo(String title, int top) {
		boolean isHighScore = updateScore();

		TextActor text1 = new TextActor(title);
		TextActor text2 = new TextActor("Pontos: " + String.valueOf(lastScore));
		TextActor text3 = new TextActor("HIGH SCORE!");

		addObject(text1, 200, top);
		addObject(text2, 200, top + 50);
		if (isHighScore) addObject(text3, 200, top + 100);
	}

	public void showGameOver() {
		showInfo("Você perdeu...", 50);
	}

	public void showPostGame() {
		showInfo("Você ganhou!", 200);

		currentLevel++; 
	}


	public void incrementScore(Class cls) {
		if (getGameState() == GameState.Game) score.incrementScore(cls);
	}

	public void act() {
		GameState current = getGameState();

		super.act();

		if (current != GameState.Game) {
			if (Greenfoot.isKeyDown("space") && !changingState) {
				changingState = true;
				changeState(current.getNextState());
			}
		}
		
		if (!Greenfoot.isKeyDown("space")) changingState = false;
	}

	public void setGameState(GameState newGameState) {
		this.gameState = newGameState;
	}
	
	public GameState getGameState() {
		return this.gameState;
	}

	protected GreenfootImage fullBackground() {
		if (gameState == null) return GameState.Intro1.getBackground();
		else return gameState.getBackground();
	}




}
