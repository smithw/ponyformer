import greenfoot.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class ActorPosition implements Box {
	private int centerX, centerY;
	private ExtendedActor actor;

	public ActorPosition(ActorPosition ap) {
		this.actor = ap.actor;
		this.centerY = ap.centerY;
		this.centerX = ap.centerX;
	}

	public ActorPosition(ExtendedActor actor, int x, int y) {
		this.actor = actor;

		setTopY(y);
		setLeftX(x);
	}

	public void setTopY(int topY) {
		centerY = topY + actor.getHeight() / 2;
	}

	public void setLeftX(int leftX) {
		centerX = leftX + actor.getWidth() / 2;
	}

	public int getLeftX() {
		return centerX - getWidth() / 2;
	}

	public int getRightX() {
		return centerX + getWidth() / 2;
	}

	public int getCenterX() {
		return centerX;
	}

	public int getTopY() {
		return centerY - getHeight() / 2;
	}

	public int getBottomY() {
		return centerY + getHeight() / 2;
	}

	public int getCenterY() {
		return centerY;
	}

	public int getWidth() {
		return actor.getWidth();
	}

	public int getHeight() {
		return actor.getHeight();
	}

	public void addToWorld(World w) {
		w.addObject(actor, centerX, centerY);
	}

	public Box addAfter(Box b, int offset) {
		return (Box) (new ActorPosition(actor, b.getRightX() + offset, getTopY())); 
	}

	public Box addAfter(Box b) {
		return addAfter(b, 0);
	}

	public Box addOver(Box b, int offset) {
		return (Box) (new ActorPosition(actor, getLeftX(), b.getTopY() - getHeight() - offset));
	}

	public Box addOver(Box b) {
		return addOver(b, 0);
	}

	public Box addOnFloor(World w, int offset) {
		return (Box) (new ActorPosition(actor, getLeftX(), w.getHeight() - getHeight()));
	}

	public Box addOnFloor(World w) {
		return addOnFloor(w, 0);
	}




}