import greenfoot.*;

public class ApplejackPower extends Power {
	private static long lastUse = -1;
	private static long delay = 500;
	private static GreenfootSound sfx = null;

	protected GreenfootSound getSFX() {
		if (sfx == null) sfx = new GreenfootSound("applejack-power.mp3");

		return sfx;
	}


	protected boolean shouldDeactivate(Pony actor) {
		if (lastUse == -1) return false;

		return (actor.getTime() - lastUse) > delay;
	}

	protected boolean isUseable(Pony actor) {
		return actor.getPowerReleased();
	}

	protected void perform(Pony actor) {
		lastUse = actor.getTime();
		actor.setIsInvincible(true);
	}

	protected void unperform(Pony actor) {
		actor.setIsInvincible(false);
	}

	public void deactivate(Pony actor) {
		if (shouldDeactivate(actor)) super.deactivate(actor);
	}
	
}