import greenfoot.*;
import java.util.Map;
import java.util.HashMap;

public enum PonyType {
	Twilight    ("TS-r.png", "TS-p.png"),
	Fluttershy  ("F-r.png",  "F-p.png"),
	PinkiePie   ("PP-r.png", "PP-p.png"),
	Rarity      ("R-r.png",  "R-p.png"),
	RainbowDash ("RD-r.png", "RD-p.png"),
	Applejack   ("AJ-r.png", "AJ-p.png");


	private String regularImg, powerImg;
	private static Map<String, GreenfootImage> imageMap = null;

	PonyType(String regularImg, String powerImg) {
		this.regularImg = regularImg;
		this.powerImg = powerImg;
	}

	private static GreenfootImage getImageForString(String imgName) {
		if (imageMap == null) imageMap = new HashMap<String, GreenfootImage>();
		if (!imageMap.containsKey(imgName)) imageMap.put(imgName, new GreenfootImage(imgName));

		return imageMap.get(imgName);
	}

	public GreenfootImage getRegularImage() {
		return getImageForString(regularImg);
	}

	public GreenfootImage getPowerImage() {
		return getImageForString(powerImg);
	}

	public Power getPower() {
		if (this == Twilight) return new TwilightPower();
		else if (this == Fluttershy) return new FluttershyPower();
		else if (this == PinkiePie) return new PinkiePiePower();
		else if (this == Rarity) return new RarityPower();
		else if (this == RainbowDash) return new RainbowDashPower();
		else return new ApplejackPower();
	}
}