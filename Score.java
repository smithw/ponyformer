import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Font;
import java.awt.Color;
import java.lang.Class;
import java.util.Map;
import java.util.HashMap;

/**
 * Write a description of class Score here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Score extends Actor {
	/**
	 * Act - do whatever the Score wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	private int score;
	private Object scoreLock;

	private static Map<Class, Integer> pointValues;

	private static Integer getPointValue(Class key) {
		if (pointValues == null) {
			pointValues = new HashMap<Class, Integer>();

			pointValues.put(Changeling.class, 20);
			pointValues.put(Jewel.class,      10);
		}

		return pointValues.get(key);
	}

	public Score(int startAt) {
		super();

		GreenfootImage img = new GreenfootImage(300, 70);
		Font f = new Font("Impact", Font.BOLD, 36);

		img.setFont(f);

		setImage(img);

		scoreLock = new Object();
		setScore(startAt);
	}

	public void setScore(int newScore) {
		synchronized (scoreLock) {
			this.score = newScore;
		}
	}
	
	public int getScore() {
		int score;

		synchronized (scoreLock) {
			score = this.score;
		}

		return score;
	}

	public void incrementScore(Class cls) {
		setScore(getScore() + getPointValue(cls).intValue());
	}

	public String getFormattedScore() {
		return String.format("%08d", getScore());
	}

	public void act() {
		GreenfootImage img = getImage();

		img.clear();
		img.setColor(Color.black);
		img.drawString("SCORE", 28, 30);
		img.setColor(new Color(109, 5, 147));
		img.drawString(getFormattedScore(), 0, 65);
		setImage(img);
	}    
}
