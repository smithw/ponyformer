import greenfoot.*;

public class PinkiePiePower extends Power {
	private static GreenfootSound sfx = null;

	protected GreenfootSound getSFX() {
		if (sfx == null) sfx = new GreenfootSound("pinkiepie-power.mp3");

		return sfx;
	}

	protected boolean shouldDeactivate(Pony actor) {
		return actor.getIsStanding();
	}

	protected void perform(Pony actor) {
		if (actor.getIsStanding()) {
			actor.getMotion(Axis.Y).setSpeed(-2*PonyVille.kJumpHeight);
		}
	}

	protected void unperform(Pony actor) {
		
	}
	
}