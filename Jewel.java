import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

/**
 * Write a description of class Jewel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Jewel extends ExtendedActor {
	/**
	 * Act - do whatever the Jewel wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	public Jewel() {
		Random rng = new Random();
		int color = rng.nextInt(6) + 1;

		setImage("joia" + color + ".gif");

	}

	public List<Class> collidesWith() {
		return new ArrayList<Class>();
	}
}
