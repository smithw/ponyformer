import java.util.*;
import java.lang.*;
import java.text.DecimalFormat;

public class Pair<A, B> {
	private final A first;
	private final B second;

	public Pair(A first, B second) {
		this.first = first;
		this.second = second;
	}

	public int hashCode() {
		int first, second;

		first = (this.first == null) ? 0 : this.first.hashCode();
		second = (this.second == null) ? 0 : this.second.hashCode();

		return bijectionN2toN(first, second);
	}

	private int bijectionN2toN(int m, int n) {
		m++;

		return ((int) Math.pow(2, m-1)) * (2*n + 1);
	}

	public boolean equals(Object obj) {
		boolean equal, firstEqual, secondEqual;
		Pair<?, ?> other;

		equal = false;

		if (obj instanceof Pair) {
			other = (Pair<?, ?>) obj;

			firstEqual = ((this.first == null) ^ (other.first == null)) ?
				false :
				((this.first == null) || this.first.equals(other.first));

			secondEqual = ((this.second == null) ^ (other.second == null)) ?
				false :
				((this.second == null) || this.second.equals(other.second));

			equal = firstEqual && secondEqual;
		}

		return equal;
	}

	public A getFirst() {
		return this.first;
	}

	public B getSecond() {
		return this.second;
	}

	public String toString() {
		return "(" + this.first + "," + this.second + ")";
	}
}