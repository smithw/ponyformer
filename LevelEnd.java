import greenfoot.*;
import java.util.List;
import java.util.ArrayList;

public class LevelEnd extends ExtendedActor {
	
	public LevelEnd() {
		setImage(new GreenfootImage(50, 800));
	}

	public List<Class> collidesWith() {
		return new ArrayList<Class>();
	}

	public List<Boundary> getCollisionBoundaries(Axis axis) {
		return new ArrayList<Boundary>();
	}
}