import greenfoot.*;
import java.util.Map;
import java.util.HashMap;

public enum GameState {
	Intro1    ("intro1.png", "intro.mp3"),
	Intro2    ("intro2.png", "intro.mp3"),
	Game      ("rainbow_wallpaper_9ad1d.jpg", "game.mp3"),
	PostGame  ("postgame.png", "intro.mp3"),
	GameOver  ("gameover.png", "intro.mp3");

	private GreenfootImage background;
	private GreenfootSound bgm;
	private String bgmName;

	private static Map<String, GreenfootSound> soundMap = null;

	GameState(String bgImg, String bgSnd) {
		background = new GreenfootImage(bgImg);
		bgmName = bgSnd;
		bgm = getBGMFromString(bgmName);
	}

	private static GreenfootSound getBGMFromString(String bgmName) {
		if (soundMap == null) {
			soundMap = new HashMap<String, GreenfootSound>();
		}

		if (!soundMap.containsKey(bgmName)) {
			GreenfootSound newSound = new GreenfootSound(bgmName);
			soundMap.put(bgmName, newSound);
		}

		return soundMap.get(bgmName);		
	}

	public GreenfootImage getBackground() {
		return this.background;
	}

	public GreenfootSound getBGM() {
		return this.bgm;
	}

	public String getBGMName() {
		return this.bgmName;
	}

	public GameState getNextState() {
		if (this == Intro1) return Intro2;
		else if (this == Game) return PostGame;
		else return Game;
	}
}