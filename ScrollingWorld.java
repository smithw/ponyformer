import greenfoot.*;
import java.util.List;
import java.util.ArrayList;
import java.lang.IllegalStateException;
import java.util.Collection;

public abstract class ScrollingWorld extends World {
	List<ExtendedActor> movingActors;
	AnchorActor anchor;
	private int position, start, end;

	protected abstract GreenfootImage fullBackground();

	public ScrollingWorld(int width, int height, int cellSize) {
		// Mundo unbounded permite que objetos fiquem fora da tela de exibição
		super(width, height, cellSize, false);

		drawBackground();
		movingActors = new ArrayList<ExtendedActor>();
		setAnchor(null);
		setPosition(0);
		setStart(0);
		setEnd(0);
	}

	public void setStart(int newStart) {
		this.start = newStart;
	}
	
	public int getStart() {
		return this.start;
	}

	public void act() {
		drawBackground();
	}

	/*
	 * Valores para essa propriedade:
	 *  -1 = Ilimitado
	 *   0 = Limitado pela imagem de fundo
	 *  >0 = Tamanho especificado
	 */
	public void setEnd(int newEnd) {
		this.end = newEnd;
	}
	
	public int getEnd() {
		return (this.end == 0) ? fullBackground().getWidth() - getWidth() + getStart() : this.end;
	}

	public void setPosition(int newPosition) {
		this.position = newPosition;
	}
	
	public int getPosition() {
		return this.position;
	}

	public List<ExtendedActor> getMovingActors() {
		return this.movingActors;
	}

	private void setAnchor(AnchorActor newAnchor) {
		this.anchor = newAnchor;

		if (newAnchor != null) {
			setStart(anchor.getX());
			setPosition(anchor.getX());
		}
	}
	
	public AnchorActor getAnchor() {
		return this.anchor;
	}

	public void addObject(AnchorActor actor, int x, int y) throws IllegalStateException {
		if (getAnchor() != null) throw new IllegalStateException("Anchor already defined.");

		super.addObject(actor, x, y);
		setAnchor(actor);
	}

	public void addObject(Actor a, int x, int y) {
		if (a instanceof AnchorActor) addObject((AnchorActor) a, x, y);
		else if (a instanceof ExtendedActor) addObject((ExtendedActor) a, x, y);
		else super.addObject(a, x, y);
	}

	public void addObject(ExtendedActor a, int x, int y) {
		movingActors.add(a);

		super.addObject(a, x, y);
	}

	public void removeObject(Actor actor) {
		if (actor instanceof AnchorActor) removeObject((AnchorActor) actor);
		else if (actor instanceof ExtendedActor) removeObject((ExtendedActor) actor);
		else super.removeObject(actor);
	}

	public void removeObject(AnchorActor actor) {
		setAnchor(null);

		super.removeObject(actor);
	}

	public void removeObject(ExtendedActor actor) {
		movingActors.remove(actor);

		super.removeObject(actor);
	}

	public void removeObjects(Collection collection) {
		for (Object obj : collection) {
			if (obj instanceof AnchorActor) setAnchor(null);
			else if (obj instanceof ExtendedActor) movingActors.remove(obj);
		}

		super.removeObjects(collection);
	}

	public void scrollBy(int offset) {
		// Offset positivo -> personagem anda para direita, resto do mundo para esquerda
		// "      negativo -> personagem anda para esquerda, resto do mundo para direita

		if (offset != 0) {
			for (ExtendedActor a : getMovingActors()) {
				Motion m = a.getMotion(Axis.X);

				m.setPosition(m.getPosition() - offset);
			}

			setPosition(getPosition() + offset);
		}
	}

	protected void drawBackground() {
		GreenfootImage fullBg = fullBackground();
		GreenfootImage trueBg = getBackground();
		int imgWidth, worldWidth, drawingPosition, occupiedScreen;

		imgWidth = fullBg.getWidth();
		worldWidth = getWidth();

		drawingPosition = (getStart() - getPosition()) % imgWidth;
		occupiedScreen = imgWidth + drawingPosition;

		if (occupiedScreen > worldWidth) occupiedScreen = worldWidth;

		trueBg.drawImage(fullBg, drawingPosition, 0);

		if (occupiedScreen < worldWidth) {
			trueBg.drawImage(fullBg, occupiedScreen, 0);
		}
	}

}