import greenfoot.*;
import java.util.List;

public class UniformlyAcceleratedMotion extends AcceleratedMotion {
	public UniformlyAcceleratedMotion(ExtendedActor actor, Axis axis) {
		super(actor, axis);
	}

	protected final double speedEquation(long deltaT) {
		double speed = getSpeed() + getAcceleration() * (double) deltaT;

		return speed;
	}
}
