import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;

/**
 * Write a description of class Changeling here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Changeling extends FacingActor {
	/**
	 * Act - do whatever the Changeling wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */
	private int facing;

	private static GreenfootSound killSfx = null;

	private static GreenfootSound getKillSfx() {
		if (killSfx == null) {
			killSfx = new GreenfootSound("kill.mp3");
			killSfx.setVolume(80);
		}

		return killSfx;
	}
	
	public Changeling() {
		super(-1);

		xMotion = new UniformLinearMotion(this, Axis.X);
		yMotion = new UniformlyAcceleratedMotion(this, Axis.Y);
		((UniformlyAcceleratedMotion) yMotion).setAcceleration(PonyVille.kDefaultG);
		xMotion.setSpeed(-PonyVille.kDefaultSpeed / 2);
	}


	public List<Class> collidesWith() {
		List<Class> collisions = new ArrayList<Class>();

		collisions.add(Platform.class);
		collisions.add(Pony.class);
		collisions.add(MonsterBouncer.class);

		return collisions;
	}

	public void toggleDirection() {
		faceDirection(-1 * getFacing());
		xMotion.setSpeed(getFacing() * PonyVille.kDefaultSpeed / 2);
	}

	public void collide(MonsterBouncer other) {
		toggleDirection();
	}

	public void collide(Pony other) {
		if (other.getIsInvincible()) triggerDeath();
	}

	public void startedDying() {
		super.startedDying();

		PonyVille w = (PonyVille) getWorld();
		GreenfootSound sfx = getKillSfx();

		if (sfx.isPlaying()) sfx.stop();
		sfx.play();
		
		w.incrementScore(getClass());
	}

	public void finishedDying() {
		super.finishedDying();
		
		getWorld().removeObject(this);
	}
}
