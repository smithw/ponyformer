import greenfoot.*;

public class TwilightPower extends Power {
	private static long lastUse = -1;
	private static long delay = 300;

	private static GreenfootSound sfx = null;

	protected GreenfootSound getSFX() {
		if (sfx == null) {
			sfx = new GreenfootSound("twilight-power.mp3");
			sfx.setVolume(85);
			// o som da Twilight está mais baixo
		}

		sfx.setVolume(85);
		return sfx;
	}

	protected boolean shouldDeactivate(Pony actor) {
		if (lastUse == -1) return false;

		return (actor.getTime() - lastUse) > delay;
	}

	protected boolean isUseable(Pony actor) {
		return actor.getPowerReleased();
	}

	protected void perform(Pony actor) {
		PonyVille w = (PonyVille) actor.getWorld();
		int direction = actor.getFacing();
		TwilightRay projectile = new TwilightRay(direction);

		lastUse = actor.getTime();

		w.addObject(projectile,
			actor.getX() + direction*(actor.getWidth() + projectile.getWidth()) / 2,
			actor.getY() - (actor.getHeight() - projectile.getHeight()) / 2
		);

	}

	protected void unperform(Pony actor) {
		
	}
}