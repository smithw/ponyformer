import greenfoot.*;

public interface Box {
	public void setTopY(int topY);
	public void setLeftX(int leftX);
	public int getLeftX();
	public int getRightX();
	public int getCenterX();
	public int getTopY();
	public int getBottomY();
	public int getCenterY();
	public int getWidth();
	public int getHeight();
	public void addToWorld(World w);
	public Box addAfter(Box b, int offset);
	public Box addAfter(Box b);
	public Box addOver(Box b);
	public Box addOver(Box b, int offset);
	public Box addOnFloor(World w);
	public Box addOnFloor(World w, int offset);
}	