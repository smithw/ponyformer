import greenfoot.*;

public class FluttershyPower extends Power {
	public static double gamma = 0.01;
	private static GreenfootSound sfx = null;

	protected GreenfootSound getSFX() {
		if (sfx == null) sfx = new GreenfootSound("fluttershy-power.mp3");

		return sfx;
	}


	protected boolean isUseable(Pony actor) {
		return !actor.getIsStanding(); 
	}

	protected boolean shouldDeactivate(Pony actor) {
		return actor.getIsStanding();
	}

	protected void perform(Pony actor) {
		Motion current;
		ParachuteMotion next;

		current = actor.getMotion(Axis.Y);
		next = current.transitionInto(ParachuteMotion.class);
		next.setGamma(gamma);

		try {
			next.setAcceleration(((AcceleratedMotion) current).getAcceleration());

			actor.setMotion(Axis.Y, next);
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}

	protected void unperform(Pony actor) {
		Motion current;
		UniformlyAcceleratedMotion next;

		current = actor.getMotion(Axis.Y);
		next = current.transitionInto(UniformlyAcceleratedMotion.class);

		try {
			next.setAcceleration(((AcceleratedMotion) current).getAcceleration());

			actor.setMotion(Axis.Y, next);
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
	
}