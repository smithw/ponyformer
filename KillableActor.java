import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;

/**
 * Write a description of class KillableActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class KillableActor extends ExtendedActor {
	/**
	 * Act - do whatever the KillableActor wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	private boolean isDead;

	public KillableActor() {
		setIsDead(false);
	}

	public void setIsDead(boolean newIsDead) {
		this.isDead = newIsDead;
	}
	
	public boolean getIsDead() {
		return this.isDead;
	}

	public void collide(Boundary b) {
		if (getIsDead() && b == Boundary.END) finishedDying();
	}

	public void triggerDeath() {
		setIsDead(true);
		startedDying();
	}

	public void startedDying() {
		yMotion.setSpeed(-5 * PonyVille.kJumpHeight / 8);
		xMotion.setSpeed(0.0);

		if (yMotion instanceof AcceleratedMotion) {
			AcceleratedMotion m = (AcceleratedMotion) yMotion;
			m.setAcceleration(m.getAcceleration() / 2);
		}

		setCanCollide(false);
		getImage().setTransparency(127);
	}

	public void finishedDying() {
		getImage().setTransparency(255);

	}

	public List<Boundary> getCollisionBoundaries(Axis axis) {
		if (axis == Axis.X) return super.getCollisionBoundaries(axis);

		if (getIsDead()) {
			List<Boundary> boundaries = new ArrayList<Boundary>();

			if (axis.get(this) - 10 - axis.getDimension(this) / 2 >= axis.getWorldMax(this))
				boundaries.add(Boundary.END);

			if (axis.get(this) - axis.getDimension(this) / 2 <= 0)
				boundaries.add(Boundary.START);

			return boundaries;
		}
		else return super.getCollisionBoundaries(axis);
	}

	protected void captureMotionInput(Axis axis) {
		if (!getIsDead()) super.captureMotionInput(axis);
	}
}
