import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;
/**
 * Write a description of class Projectile here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Projectile extends ExtendedActor {
	/**
	 * Act - do whatever the Projectile wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	protected boolean shouldDisappear;

	public Projectile(int direction) {
		xMotion = new UniformLinearMotion(this, Axis.X);
		xMotion.setSpeed(direction * PonyVille.kDefaultSpeed * 2);

		shouldDisappear = false;

		if (direction < 0) getImage().mirrorHorizontally();
	}

	public List<Class> collidesWith() {
		List<Class> collisions = new ArrayList<Class>();
		collisions.add(Changeling.class);
		collisions.add(Platform.class);
		return collisions;
	}

	protected boolean shouldCollideBoundaryAxis(Axis axis) {
		return true;
	}

	public void collide(Changeling actor) {
		actor.triggerDeath();
		disappear();
	}

	public void collide(Platform plat) {
		disappear();
	}

	public void collide(SpikeBall ball) {
		disappear();
	}

	public void collide(Boundary b) {
		disappear();
	}

	public void disappear() {
		shouldDisappear = true;
	}

	public void act() {
		super.act();

		if (shouldDisappear) getWorld().removeObject(this);
	}


}
