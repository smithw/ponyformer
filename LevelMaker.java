import greenfoot.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class LevelMaker {
	public int lastX;
	private World world;

	public LevelMaker(World w) {
		world = w;
	}

	public Box makeHills(int count, int hCount, int x) {
		ActorPositionMonoid hills;
		Box hill;

		hills = null;

		for (int j = 0; j < count; j++) {
			hill = makeHPlatforms(hCount, x, 0);

			if (hills == null) hills = new ActorPositionMonoid((ActorPositionMonoid) hill);
			else hills.addActors((ActorPositionMonoid) hill.addAfter(hills));
		}

		return (Box) hills.addOnFloor(world);
	}

	//public Box makeValley(int count, int x, boolean useSpike) {

		
	//}

	public Box makeSpike(int x, int y) {
		return (Box) (new ActorPosition(new SpikeBall(), x, y));
	}

	public Box makeSpikes(int count, int x, int y) {
		ActorPositionMonoid spikes = new ActorPositionMonoid((ActorPosition) makeSpike(x, y));

		for (int i = 0; i < (count - 1); i++)
			spikes.addActor((ActorPosition) makeSpike(x, y).addAfter(spikes));

		return spikes;
	}

	public Box makeHSpikes(int count, int x, int y) {
		ActorPositionMonoid spikes = new ActorPositionMonoid((ActorPosition) makeSpike(x, y));

		for (int i = 0; i < (count - 1); i++)
			spikes.addActor((ActorPosition) makeSpike(x, y).addOver(spikes));

		return spikes;
	}

	public Box makeJewel(int x, int y) {
		return (Box) (new ActorPosition(new Jewel(), x, y));
	}

	public Box makeJewels(int count, int x, int y) {
		ActorPositionMonoid jewels = new ActorPositionMonoid((ActorPosition) makeJewel(x, y));

		for (int i = 0; i < (count - 1); i++)
			jewels.addActor((ActorPosition) makeJewel(x, y).addAfter(jewels, 15));

		return jewels;
	}

	public Box makeHJewels(int count, int x, int y) {
		ActorPositionMonoid jewels = new ActorPositionMonoid((ActorPosition) makeJewel(x, y));

		for (int i = 0; i < (count - 1); i++)
			jewels.addActor((ActorPosition) makeJewel(x, y).addOver(jewels, 15));

		return jewels;
	}

	public Box makePlatform(int x, int y) {
		return (Box) (new ActorPosition(new Platform(), x, y));
	}

	public Box makePlatforms(int count, int x, int y) {
		ActorPositionMonoid platforms = new ActorPositionMonoid((ActorPosition) makePlatform(x, y));

		for (int i = 0; i < (count - 1); i++) 
			platforms.addActor((ActorPosition) makePlatform(x, y).addAfter(platforms));
		
		return platforms;
	}

	public Box makeHPlatforms(int count, int x, int y) {
		ActorPositionMonoid platforms = new ActorPositionMonoid((ActorPosition) makePlatform(x, y));

		for (int i = 0; i < (count - 1); i++) 
			platforms.addActor((ActorPosition) makePlatform(x, y).addOver(platforms));
		
		return platforms;
	}

	public Box makeEnemy(int x, int y) {
		return (Box) (new ActorPosition(new Changeling(), x, y));
	}

	public Box makeBouncer(int x, int y) {
		return (Box) (new ActorPosition(new MonsterBouncer(), x, y));
	}

	public Box makeEnemyWithBouncers(int x, int y, int freedom) {
		Box enemy = makeEnemy(x, y);
		Box bouncer = makeBouncer(x, y);
		int bouncerY = y + enemy.getHeight() - bouncer.getHeight();

		freedom = (freedom - enemy.getWidth()) / 2;

		((ActorPosition) bouncer).setTopY(bouncerY);

		ActorPositionMonoid compound = new ActorPositionMonoid((ActorPosition) bouncer);

		compound.addActor((ActorPosition) enemy.addAfter(compound, freedom));
		compound.addActor((ActorPosition) makeBouncer(x, bouncerY).addAfter(compound, freedom));

		return (Box) compound;
	}

	public Box makeMultipleEnemiesWithBouncers(int count, int x, int y, int freedom) {
		ActorPositionMonoid enemies = new ActorPositionMonoid((ActorPositionMonoid) makeEnemyWithBouncers(x, y, freedom));

		for (int i = 0; i < (count - 1); i++)
			enemies.addActors((ActorPositionMonoid) makeEnemyWithBouncers(x, y, freedom).addAfter(enemies));

		return (Box) enemies;
	}

	public Box makeFluttershySpikeTrap(int x) {
		ActorPositionMonoid spikeTrap = new ActorPositionMonoid((ActorPositionMonoid) makeSpikes(2, x, 200));

		spikeTrap.addActors((ActorPositionMonoid) makeHSpikes(4, 0, 0).addAfter(spikeTrap, 380));

		spikeTrap.setTopY(1);

		return (Box) spikeTrap;
	}

	public Box makeEndOfLevel(int x) {
		ActorPosition end = new ActorPosition(new LevelEnd(), x, 0);

		return (Box) end.addOnFloor(world);
	}

}





