public class RestMotion extends Motion {
	public RestMotion(ExtendedActor boundActor, Axis axis) {
		super(boundActor, axis);
	}

	protected final double speedEquation(long deltaT) {
		return 0.0;
	}

	public double getSpeed() {
		return 0.0;
	}
}