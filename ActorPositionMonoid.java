import greenfoot.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class ActorPositionMonoid implements Box {
	private List<ActorPosition> actors;

	public ActorPositionMonoid(ActorPositionMonoid actors) {
		this.actors = new ArrayList<ActorPosition>();

		addActors(actors);
	}

	public ActorPositionMonoid(List<ActorPosition> actors) {
		this.actors = new ArrayList<ActorPosition>();

		addActors(actors);
	}

	public ActorPositionMonoid(ActorPosition actor) {
		this.actors = new ArrayList<ActorPosition>();

		addActor(actor);
	}

	public ActorPositionMonoid() {
		this.actors = new ArrayList<ActorPosition>();
	}

	public ActorPositionMonoid addActor(ActorPosition actor) {
		this.actors.add(actor);

		return this;
	}

	public ActorPositionMonoid addActors(List<ActorPosition> actors) {
		this.actors.addAll(actors);

		return this;
	}

	public ActorPositionMonoid addActors(ActorPositionMonoid actors) {
		this.actors.addAll(actors.actors);

		return this;
	}

	public void setTopY(int topY) {
		int currentTopY = getTopY();

		for (ActorPosition ap : actors) {
			int pieceOffset = ap.getTopY() - currentTopY;
			ap.setTopY(topY + pieceOffset);
		}
	}

	public void setLeftX(int leftX) {
		int currentLeftX = getLeftX();

		for (ActorPosition ap : actors) {
			int pieceOffset = ap.getLeftX() - currentLeftX;
			ap.setLeftX(leftX + pieceOffset);
		}
	}

	public int getLeftX() {
		if (actors.isEmpty()) return 0;

		int leftX = actors.get(0).getLeftX();

		for (ActorPosition ap : actors) {
			if (ap.getLeftX() < leftX) leftX = ap.getLeftX();
		}

		return leftX;
	}

	public int getRightX() {
		if (actors.isEmpty()) return 0;

		int rightX = actors.get(0).getRightX();

		for (ActorPosition ap : actors) {
			if (ap.getRightX() > rightX) rightX = ap.getRightX();
		}	

		return rightX;
	}

	public int getCenterX() {
		if (actors.isEmpty()) return 0;

		return getRightX() + getWidth() / 2;
	}

	public int getTopY() {
		if (actors.isEmpty()) return 0;
		
		int topY = actors.get(0).getTopY();

		for (ActorPosition ap : actors) {
			if (ap.getTopY() < topY) topY = ap.getTopY();
		}

		return topY;
	}

	public int getBottomY() {
		if (actors.isEmpty()) return 0;

		int bottomY = actors.get(0).getBottomY();

		for (ActorPosition ap : actors) {
			if (ap.getBottomY() > bottomY) bottomY = ap.getBottomY();
		}

		return bottomY;
	}

	public int getCenterY() {
		if (actors.isEmpty()) return 0;

		return getTopY() + getHeight() / 2;
	}

	public int getWidth() {
		if (actors.isEmpty()) return 0;

		return getRightX() - getLeftX();
	}

	public int getHeight() {
		if (actors.isEmpty()) return 0;
		
		return getBottomY() - getTopY();
	}

	public Box addAfter(Box b, int offset) {
		ActorPositionMonoid moved = new ActorPositionMonoid();
		int newLeftX = b.getRightX() + offset;
		int currentLeftX = getLeftX();

		for (ActorPosition ap : actors) {
			ActorPosition newAp = new ActorPosition(ap);
			int pieceOffset = ap.getLeftX() - currentLeftX;
			newAp.setLeftX(newLeftX + pieceOffset);

			moved.addActor(newAp);
		}

		return (Box) moved;
	}

	public Box addOver(Box b, int offset) {
		ActorPositionMonoid moved = new ActorPositionMonoid();
		int newTopY = b.getTopY() - getHeight() - offset;
		int currentTopY = getTopY();

		for (ActorPosition ap : actors) {
			ActorPosition newAp = new ActorPosition(ap);
			int pieceOffset = ap.getTopY() - currentTopY;
			newAp.setTopY(newTopY + pieceOffset);

			moved.addActor(newAp);
		}
		
		return (Box) moved;
	}

	public Box addOnFloor(World w, int offset) {
		ActorPositionMonoid moved = new ActorPositionMonoid();
		int newTopY = w.getHeight() - getHeight() - offset;
		int currentTopY = getTopY();

		for (ActorPosition ap : actors) {
			ActorPosition newAp = new ActorPosition(ap);
			int pieceOffset = ap.getTopY() - currentTopY;
			newAp.setTopY(newTopY + pieceOffset);

			moved.addActor(newAp);
		}

		return (Box) moved;
	}
	
	public Box addAfter(Box b) {
		return addAfter(b, 0);
	}

	public Box addOver(Box b) {
		return addOver(b, 0);
	}

	public Box addOnFloor(World w) {
		return addOnFloor(w, 0);
	}

	public void addToWorld(World w) {
		for (ActorPosition ap : actors) ap.addToWorld(w);
	}
}