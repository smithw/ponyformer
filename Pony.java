import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;
import java.lang.ClassCastException;

/**
 * Write a description of class Pony here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Pony extends AnchorActor {
	/**
	 * Act - do whatever the Pony wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	private long time;
	PonyType currentPony;
	private boolean isPowerActive;
	private boolean isInvincible;
	private boolean powerReleased;
	private GreenfootSound coinSfx, deathSfx;

	public Pony() {
		super();

		currentPony = null;
		setCurrentPony(PonyType.Twilight);
		setPowerReleased(false);

		coinSfx = new GreenfootSound("coin2.mp3");
		deathSfx = new GreenfootSound("player-death.mp3");

		xMotion = new UniformLinearMotion(this, Axis.X);
		yMotion = new UniformlyAcceleratedMotion(this, Axis.Y);
		((UniformlyAcceleratedMotion) yMotion).setAcceleration(PonyVille.kDefaultG);
	}

	public void setIsInvincible(boolean newIsInvincible) {
		this.isInvincible = newIsInvincible;
	}
	
	public boolean getIsInvincible() {
		return this.isInvincible;
	}

	public List<Class> collidesWith() {
		List<Class> causesCollision = new ArrayList<Class>();

		causesCollision.add(Changeling.class);
		causesCollision.add(Platform.class);
		causesCollision.add(Jewel.class);
		causesCollision.add(LevelEnd.class);
		return causesCollision;
	}

	public void startedDying() {
		Power p = currentPony.getPower();
		p.deactivate(this);
		faceRight();
		drawPony();
		deathSfx.play();


		super.startedDying();
	}

	public void finishedDying() {
		super.finishedDying();

		((PonyVille) getWorld()).changeState(GameState.GameOver);
	}

	public void act() {
		PonyVille w = (PonyVille) getWorld();

		if (w.getWonLastLevel())
			w.changeState(GameState.PostGame);
		else {
			if (!getIsDead()) {
				checkPonyChange();
				checkPowerUse();
			}
			super.act();

			drawPony();
		}
	}

	public void collide(Changeling actor) {
		if (!getIsInvincible()) triggerDeath();
	}

	public void collide(Jewel actor) {
		PonyVille w = (PonyVille) getWorld();

		w.removeObject(actor);
		w.incrementScore(Jewel.class);

		if (coinSfx.isPlaying()) coinSfx.stop();
		coinSfx.play();
	}

	public void collide(SpikeBall actor) {
		triggerDeath();
	}

	public void collide(LevelEnd actor) {
		triggerVictory();
	}

	public void triggerVictory() {
		PonyVille w = (PonyVille) getWorld();

		w.setWonLastLevel(true);
		setCanCollide(false);
	}

	public void drawPony() {
		int oldFacing = getFacing();

		if (getIsPowerActive()) setImage(getCurrentPony().getPowerImage());
		else setImage(getCurrentPony().getRegularImage());

		facing = 1;
		faceDirection(oldFacing);
	}

	public void setImage(GreenfootImage img) {
		if (getFacing() == -1) getImage().mirrorHorizontally();

		super.setImage(img);
	}

	public void checkPonyChange() {
		if (key("q")) setCurrentPony(PonyType.Twilight);
		if (key("w")) setCurrentPony(PonyType.Fluttershy);
		if (key("e")) setCurrentPony(PonyType.RainbowDash);
		if (key("a")) setCurrentPony(PonyType.Applejack);
		if (key("s")) setCurrentPony(PonyType.Rarity);
		if (key("d")) setCurrentPony(PonyType.PinkiePie);
	}

	protected void moveX() {
		double newSpeed = 0.0;
		int newFacing;

		if (key("left")) newSpeed -= PonyVille.kDefaultSpeed;
		if (key("right")) newSpeed += PonyVille.kDefaultSpeed;

		if (xMotion instanceof UniformLinearMotion) {
			// Se o eixo X não estiver em movimento retílineo uniforme,
			// o poder da Rainbow Dash está ativo, e não queremos
			// cancelar o poder porque o usuário esqueceu de soltar
			// a tecla de direção.

			faceDirection((int) (newSpeed / Math.abs(newSpeed)));
			xMotion.setSpeed(newSpeed);
		}
	}

	protected void moveY() {
		if (key("up")) jump();
	}

	protected void jump() {
		if (getIsStanding()) {
			yMotion.setSpeed(-PonyVille.kJumpHeight);
		}
	}

	public void setCurrentPony(PonyType p) {
		if (currentPony != null) {
			Power currentPower = currentPony.getPower();
			currentPower.deactivate(this);
		}

		currentPony = p;
	}

	public PonyType getCurrentPony() {
		return currentPony;
	}

	public void checkPowerUse() {
		Power ponyPower;

		ponyPower = getCurrentPony().getPower();

		if (key("space")) {
			ponyPower.use(this);
			setPowerReleased(false);
		}
		else {
			ponyPower.deactivate(this);
			setPowerReleased(true);
		}
	}

	public void setPowerReleased(boolean newPowerReleased) {
		this.powerReleased = newPowerReleased;
	}
	
	public boolean getPowerReleased() {
		return this.powerReleased;
	}

	public void setIsPowerActive(boolean newIsPowerActive) {
		this.isPowerActive = newIsPowerActive;
	}

	public boolean getIsPowerActive() {
		return this.isPowerActive;
	}

	public boolean getIsStanding() {
		boolean onFloor, onPlatform;
		List<Platform> inRange;

		onFloor = getY() >= getWorld().getHeight() - getHeight();

		inRange = getObjectsAtOffset(-getWidth() / 2, 1 + getHeight() / 2, Platform.class);
		inRange.addAll(getObjectsAtOffset(getWidth() / 2, 1 + getHeight() / 2, Platform.class));

		onPlatform = false;
		for (Platform p : inRange) {
			if (p.getY() > getY()) {
				onPlatform = true;
				break;
			}
		}

		return onFloor || onPlatform;
	}
}
