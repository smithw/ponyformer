import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class RarityRay here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RarityRay extends Projectile {
	/**
	 * Act - do whatever the RarityRay wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	public RarityRay(int direction) {
		super(direction);

		yMotion = new UniformlyAcceleratedMotion(this, Axis.Y);
		((UniformlyAcceleratedMotion) yMotion).setAcceleration(PonyVille.kDefaultG);
		yMotion.setSpeed(-PonyVille.kJumpHeight/6);
	}

}
