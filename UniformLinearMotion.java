import greenfoot.*;

public class UniformLinearMotion extends Motion {
	public UniformLinearMotion(ExtendedActor actor, Axis axis) {
		super(actor, axis);
	}
	
	protected final double speedEquation(long deltaT) {
		return getSpeed();
	}
}