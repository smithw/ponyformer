public abstract class AcceleratedMotion extends Motion {
	private double acceleration;
	private Object accLock;

	public AcceleratedMotion(ExtendedActor actor, Axis axis) {
		super(actor, axis);
		accLock = new Object();
	}

	public void setAcceleration(double newAcceleration) {
		synchronized (accLock) {
			this.acceleration = newAcceleration;
		}
	}
	
	public double getAcceleration() {
		double acceleration;

		synchronized (accLock) {
			acceleration = this.acceleration;
		}
		
		return acceleration;
	}
		
}