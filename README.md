# Ponyformer

**Disclaimer:** This is not authorized, endorsed or in any way related to Hasbro and any of the other copyrights owners of the images and sounds used here. If copyrighted material owned by you is somehow used in this game, feel free to tell me and I'll take it down immediately. This game is a tribute to a TV show we bronies love so much.

My Little Pony, My Little Pony: Friendship is Magic, Twilight Sparkle, Fluttershy, Rainbow Dash, Applejack, Rarity, Pinkie Pie and others are copyrighted material owned by Hasbro.