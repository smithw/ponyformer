import greenfoot.*;
import java.lang.Class;
import java.lang.reflect.Method;
import java.lang.NoSuchMethodException;
import java.lang.IllegalAccessException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.ArrayList;

public abstract class ExtendedActor extends Actor {
	private double mass;
	protected Motion xMotion, yMotion;
	protected long time;
	protected long deltaT;
	protected boolean canCollide;

	ExtendedActor() {
		xMotion = new RestMotion(this, Axis.X);
		yMotion = new RestMotion(this, Axis.Y);
		
		time = -1;
		deltaT = 0;
		setCanCollide(true);
	}

	public void setCanCollide(boolean newCanCollide) {
		this.canCollide = newCanCollide;
	}
	
	public boolean getCanCollide() {
		return this.canCollide;
	}

	protected void addedToWorld(World w) {
		xMotion.resetPosition();
		yMotion.resetPosition();
	}

	protected boolean key(String keyName) {
		return Greenfoot.isKeyDown(keyName);
	}

	public void updateTime() {
		long now = System.currentTimeMillis();

		deltaT = (time == -1) ? 0 : now - time;
		time = now;
	}

	public long getTime() {
		return time;
	}

	public long getDeltaT() {
		return deltaT;
	}

	public void act() {
		updateTime();
		moveAxis(Axis.X);
		moveAxis(Axis.Y);
	}

	protected void captureMotionInput(Axis axis) {
		if (axis == Axis.X) moveX();
		else if (axis == Axis.Y) moveY();
	}

	protected void moveAxis(Axis axis) {
		Motion m = getMotion(axis);
		CollisionType collisionType, cursor;
		Object collisionObject = null;

		captureMotionInput(axis);

		m.step(getDeltaT());

		collisionType = checkCollision(axis);

		if (collisionType == CollisionType.BOUNDARY)
			collisionObject = getCollisionBoundaries(axis).get(0);
		else if (collisionType == CollisionType.ACTOR)
			collisionObject = getCollisionActors().get(0);

		while ((cursor = checkCollision(axis)) != CollisionType.NONE) {
			try {
				m.stepBack(cursor);
			}
			catch (IllegalStateException e) {
				// a velocidade original era 0, portanto não há como voltar
				break;
			}
		}

		if (collisionType != CollisionType.NONE) {
			m.setSpeed(0.0);
			if (collisionObject != null) collide(collisionObject);
		}
		else didntCollide();
	}

	public Motion getMotion(Axis axis) {
		if (axis == Axis.X) return xMotion;
		if (axis == Axis.Y) return yMotion;
		return null;
	}

	public void setMotion(Axis axis, Motion newMotion) {
		if (axis == Axis.X) xMotion = newMotion;
		if (axis == Axis.Y) yMotion = newMotion;
	}

	public void setX(Integer x) {
		setX(x.intValue());
	}

	public void setY(Integer y) {
		setY(y.intValue());
	}

	public void setX(int x) {
		setLocation(x, getY());
	}

	public void setY(int y) {
		setLocation(getX(), y);
	}

	protected boolean shouldCollideBoundaryAxis(Axis axis) {
		return (axis == Axis.Y); // Laterais horizontais permitem saída dos atores
	}

	public List<Boundary> getCollisionBoundaries(Axis axis) {
		List<Boundary> boundaries = new ArrayList<Boundary>();

		if (!shouldCollideBoundaryAxis(axis)) return boundaries;

		if (axis.get(this) + axis.getDimension(this) / 2 >= axis.getWorldMax(this))
			boundaries.add(Boundary.END);
		if (axis.get(this) - axis.getDimension(this) / 2 <= 0)
			boundaries.add(Boundary.START);

		return boundaries;
	}

	public List<ExtendedActor> getCollisionActors() {
		List<ExtendedActor> actors = new ArrayList<ExtendedActor>();
		List<ExtendedActor> collidable;

		for (Class c : collidesWith()) {
			collidable = getIntersectingObjects(c);

			for (ExtendedActor actor : collidable) {
				if (actor.getCanCollide()) actors.add(actor);
			}
		}

		return actors;
	}

	public CollisionType checkCollision(Axis axis) {
		List<ExtendedActor> actors;
		List<Boundary> boundaries;

		actors = getCollisionActors();

		if (!actors.isEmpty() && getCanCollide()) return CollisionType.ACTOR;

		boundaries = getCollisionBoundaries(axis);

		if (!boundaries.isEmpty()) return CollisionType.BOUNDARY;

		return CollisionType.NONE;
	}

	public int getWidth() {
		return getImage().getWidth();
	}

	public int getHeight() {
		return getImage().getHeight();
	}

	public abstract List<Class> collidesWith();

	public void collide(Object obj) {
		Method collMethod;

		try {
			Class objCls = obj.getClass();

			if (objCls != Object.class) {
				collMethod = this.getClass().getMethod("collide", objCls);
				collMethod.invoke(this, objCls.cast(obj));
			}
		} 
		catch (NoSuchMethodException e) {
			// Não faça nada.
		}
		catch (IllegalAccessException e) {
			// Não faça nada.
		}
		catch (InvocationTargetException e) {
			// Não faça nada.
		}
	}

	public void didntCollide() {

	}

	protected void moveX() {

	}

	protected void moveY() {

	}


}