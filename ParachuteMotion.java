public class ParachuteMotion extends AcceleratedMotion {
	private double gamma;

	public ParachuteMotion(ExtendedActor actor, Axis axis) {
		super(actor, axis);
	}

	public void setGamma(double newGamma) {
		this.gamma = newGamma;
	}

	public double getGamma() {
		return this.gamma;
	}

	protected final double speedEquation(long deltaT) {
		double s0 = getSpeed();
		double gamma = getGamma();
		double gOverGamma = getAcceleration() / gamma;

		return (s0 - gOverGamma) * Math.exp(-gamma*deltaT) + gOverGamma;
	}
	
}