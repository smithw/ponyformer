import greenfoot.*;
import java.util.List;
import java.util.ArrayList;

public abstract class AnchorActor extends FacingActor {
	public void setX(int x) {
		World w = getWorld();

		if (ScrollingWorld.class.isInstance(w)) {
			ScrollingWorld sw = (ScrollingWorld) w;

			int delta = x - sw.getPosition();

			sw.scrollBy(delta);
		}
		else super.setX(x);
	}

	public List<Boundary> getCollisionBoundaries(Axis axis) {
		World w = getWorld();

		if (axis == Axis.Y) return super.getCollisionBoundaries(axis);

		if (ScrollingWorld.class.isInstance(w)) {
			List<Boundary> boundaries = new ArrayList<Boundary>();
			ScrollingWorld sw = (ScrollingWorld) w;

			if (sw.getPosition() <= sw.getStart()) boundaries.add(Boundary.START);

			if (sw.getEnd() > 0) {
				if (sw.getPosition() >= sw.getEnd()) boundaries.add(Boundary.END);
			}

			return boundaries;
		}
		else return super.getCollisionBoundaries(axis);

	}
}