import greenfoot.*;
import java.lang.Class;
import java.lang.reflect.Method;

public enum Axis {
	X("getX", "setX", "getWidth"),
	Y("getY", "setY", "getHeight");

	private String getter, setter, dimension;

	Axis(String getter, String setter, String dimension) {
		this.getter = getter;
		this.setter = setter;
		this.dimension = dimension;
	}

	public void set(ExtendedActor actor, int value) {
		try {
			Method m = ExtendedActor.class.getMethod(setter, Integer.class);
			m.invoke(actor, value);
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}

	public int get(ExtendedActor actor) {
		try {
			Method m = ExtendedActor.class.getMethod(getter);
			return ((Integer) m.invoke(actor)).intValue();
		}
		catch (Exception e) {
			// should not happen.
			System.out.println(e);
			return 0;
		}
	}

	public int getWorldMax(ExtendedActor actor) {
		World w = actor.getWorld();

		try {
			Method m = World.class.getMethod(dimension);
			return ((Integer) m.invoke(w)).intValue();
		}
		catch (Exception e) {
			// should not happen.
			System.out.println(e);
			return 0;
		}
	}

	public int getDimension(ExtendedActor actor) {
		try {
			Method m = ExtendedActor.class.getMethod(dimension);
			return ((Integer) m.invoke(actor)).intValue();
		}
		catch (Exception e) {
			// should not happen.
			System.out.println(e);
			return 0;
		}
	}
}