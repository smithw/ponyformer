import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;

/**
 * Write a description of class Platform here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Platform extends ExtendedActor {
	/**
	 * Act - do whatever the Platform wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	public List<Class> collidesWith() {
		List<Class> collisions = new ArrayList<Class>();
		collisions.add(Pony.class);
		collisions.add(Changeling.class);
		return collisions;
	}
}
