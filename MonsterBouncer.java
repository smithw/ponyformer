import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;
/**
 * Write a description of class MonsterBouncer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MonsterBouncer extends ExtendedActor {
	/**
	 * Act - do whatever the MonsterBouncer wants to do. This method is called whenever
	 * the 'Act' or 'Run' button gets pressed in the environment.
	 */

	public List<Class> collidesWith() {
		List<Class> collisions = new ArrayList<Class>();
		collisions.add(Changeling.class);
		return collisions;
	}

}
