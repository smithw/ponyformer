import greenfoot.*;
import java.awt.Font;
import java.awt.Color;

public class TextActor extends Actor {
	private String nextText;
	private String currentText;
	private Color color;

	public TextActor(String text) {
		super();

		GreenfootImage img = new GreenfootImage(text.length() * 20 + 10, 40);
		Font f = new Font("Impact", Font.BOLD, 36);

		img.setFont(f);
		setColor(Color.black);
		setImage(img);

		setText(text);
		currentText = "";
	}

	public void setColor(Color newColor) {
		this.color = newColor;
	}
	
	public Color getColor() {
		return this.color;
	}

	public void setText(String newText) {
		this.nextText = newText;
	}
	
	public String getText() {
		return this.nextText;
	}

	public void act() {
		if (nextText != currentText) {
			currentText = nextText;
			updateImage();
		}
	}

	public void updateImage() {
		GreenfootImage img = getImage();

		img.clear();
		img.setColor(getColor());
		img.drawString(currentText, 0, 30);
		
		setImage(img);
	}
}